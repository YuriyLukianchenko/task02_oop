package view;

import controller.Controller;
import model.DwellingManager;
import java.util.Scanner;
import static model.SortArgument.*;

/**
 *
 */
public class Menu {
    public void start(){
        DwellingManager dwellingmanager = new DwellingManager();
        Controller controller = new Controller();
        controller.createDefaultListOfDwellings(dwellingmanager);
        System.out.println("FIND YOUR DWELLING");
        showInfoAboutOptions();
        Scanner sc = new Scanner(System.in);
        label:
        while (true){
            int number = sc.nextInt();
            switch(number){
                case 1:
                    controller.showDWellings(dwellingmanager);
                    break;
                case 2:
                    controller.sortBy(dwellingmanager,PRICE);
                    System.out.println("Sorting was performed");
                    break;
                case 3:
                    controller.sortBy(dwellingmanager,MARKET);
                    System.out.println("Sorting was performed");
                    break;
                case 4:
                    controller.sortBy(dwellingmanager,STADIUM);
                    System.out.println("Sorting was performed");
                    break;
                case 5: controller.sortBy(dwellingmanager,THEATRE);
                    System.out.println("Sorting was performed");
                    break;
                case 6:
                    System.out.println(controller.showAllDwellingsTopPriceTopDistancetoMarket(dwellingmanager,300, 3));
                    break;
                case 7:
                    break label;
            }
        }
    }

    private void showInfoAboutOptions(){
        System.out.println("You can perform next actions:");
        System.out.println("1 - show all dwellings");
        System.out.println("2 - sort dwellings by price");
        System.out.println("3 - sort dwellings by distanceToMarket");
        System.out.println("4 - sort dwellings by distanceToStadium");
        System.out.println("5 - sort dwellings by distanceToTheatre");
        System.out.println("6 - show optimal dwellings with Price lower than 300 and distanceToMarket lowwer than 3 km");
        System.out.println("7 - quit");
    }
}
