package model;

/**
 *
 */
public class House extends Dwelling {
    /**
     *
     */
    private int floors;
    /**
     *
     * @return
     */
    public int getFloors() {
        return floors;
    }

    /**
     *
     * @param floors
     */
    public void setFloors(int floors) {
        this.floors = floors;
    }
}
