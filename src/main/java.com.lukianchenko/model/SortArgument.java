package model;

public enum SortArgument {
    PRICE, MARKET, THEATRE, STADIUM
}
