package model;

/**
 *
 */
public class Dwelling {
    /**
     *
     */
    private String address;
    /**
     *
     */
    private double price;
    /**
     *
     */
    private int distanceToMarket;
    /**
     *
     */
    private int distanceToStadium;
    /**
     *
     */
    private int distanceToTheatre;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getDistanceToMarket() {
        return distanceToMarket;
    }

    public void setDistanceToMarket(int distanceToMarket) {
        this.distanceToMarket = distanceToMarket;
    }

    public int getDistanceToStadium() {
        return distanceToStadium;
    }

    public void setDistanceToStadium(int distanceToStadium) {
        this.distanceToStadium = distanceToStadium;
    }

    public int getDistanceToTheatre() {
        return distanceToTheatre;
    }

    public void setDistanceToTheatre(int distanceToTheatre) {
        this.distanceToTheatre = distanceToTheatre;
    }

    /**
     *
     * @param dwell
     */
    public void calculatePrice(Dwelling dwell){
        dwell.price = (1/((double)dwell.distanceToMarket) * 4/((double)dwell.distanceToTheatre) * 4/((double)dwell.distanceToStadium)) * 100 ;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString(){
        return "Dwelling.address: " + address + "; price = " + price  +
                "; distanceToMarket = " + distanceToMarket +
                "; distanceToTheatre = " + distanceToTheatre +
                "; distanceToStadium = " + distanceToStadium +
                  "\n";
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj){

        if (obj instanceof Dwelling){
            Dwelling dwell = (Dwelling)obj;
            if((dwell.address == this.address) &&
                    (dwell.distanceToMarket == this.distanceToMarket) &&
                    (dwell.distanceToStadium == this.distanceToStadium) &&
                    (dwell.distanceToTheatre == this.distanceToTheatre)){
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode(){
        return address.length() + distanceToMarket * 10 +
                distanceToStadium * 100 + distanceToTheatre * 1000;
    }


}
