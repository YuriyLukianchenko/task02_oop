package model;

/**
 *
 */
public class PentHouse extends Dwelling {
    private boolean minibar;

    /**
     *
     * @return
     */
    public boolean isMinibar() {
        return minibar;
    }

    /**
     *
     * @param minibar
     */
    public void setMinibar(boolean minibar) {
        this.minibar = minibar;
    }
}
