package model;

/**
 *
 */
public class Flat extends Dwelling {
    /**
     *
     */
    private int rooms;

    /**
     *
     * @return
     */
    public int getRooms() {
        return rooms;
    }

    /**
     *
     * @param rooms
     */
    public void setRooms(int rooms) {
        this.rooms = rooms;
    }
}
