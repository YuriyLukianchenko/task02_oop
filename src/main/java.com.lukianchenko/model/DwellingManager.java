package model;

import model.myException.UnknownDwellingType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 */
public class DwellingManager {
    /**
     *
     */
    private List<Dwelling> list = new ArrayList<>();

    /**
     *
     * @return
     */
    public List<Dwelling> getList() {
        return list;
    }

    /**
     *
     * @param list
     */
    public void setList(List<Dwelling> list) {
        this.list = list;
    }

    /**
     *
     * @param list
     * @return
     */
    public void sortByPrice(List<Dwelling> list){
        Collections.sort(list, new Comparator<Dwelling>() {
            public int compare(Dwelling d1, Dwelling d2) {
                return d1.getPrice() < d2.getPrice() ? -1 : d1.getPrice() == d2.getPrice() ? 0 : 1;
            }
        });
    }

    /**
     *
     * @return
     */
    public void sortByDistanceToMarket(List<Dwelling> list){
        Collections.sort(list, new Comparator<Dwelling>() {
            public int compare(Dwelling d1, Dwelling d2) {
                return d1.getDistanceToMarket() < d2.getDistanceToMarket() ?
                        -1 : d1.getDistanceToMarket() == d2.getDistanceToMarket() ? 0 : 1;
            }
        });
    }

    /**
     *
     * @param list
     */
    public void sortByDistanceToTheatre(List<Dwelling> list){
        Collections.sort(list, new Comparator<Dwelling>() {
            public int compare(Dwelling d1, Dwelling d2) {
                return d1.getDistanceToTheatre() < d2.getDistanceToTheatre() ?
                        -1 : d1.getDistanceToTheatre() == d2.getDistanceToTheatre() ? 0 : 1;
            }
        });
    }

    /**
     *
     * @param list
     */
    public void sortByDistanceToStadium(List<Dwelling> list){
        Collections.sort(list, new Comparator<Dwelling>() {
            public int compare(Dwelling d1, Dwelling d2) {
                return d1.getDistanceToStadium() < d2.getDistanceToStadium() ?
                        -1 : d1.getDistanceToStadium() == d2.getDistanceToStadium() ? 0 : 1;
            }
        });
    }

    public List<Dwelling> findOptimaDwelling(DwellingManager dwellMan, int maxPrice, int MaxDistanceToMarket){
        dwellMan.sortByDistanceToMarket(dwellMan.getList());
        List<Dwelling> list = new ArrayList<>();
        for(Dwelling dwell: dwellMan.getList()){
            if (dwell.getDistanceToMarket() <= MaxDistanceToMarket){
                list.add(dwell);
            }
            else {
                break;
            }
        }
        DwellingManager dwellBuffer = new DwellingManager();
        dwellBuffer.getList().clear();
        dwellBuffer.getList().addAll(list);
        list.clear();
        dwellBuffer.sortByPrice(dwellBuffer.getList());
        for(Dwelling dwell: dwellBuffer.getList()){
            if (dwell.getPrice() <= maxPrice){
                list.add(dwell);
            }
            else {
                break;
            }
        }
        return list;
    }

    /**
     *
     * @return
     */
    private Dwelling createDwelling (String type, String address, int distanceToMarket, int distanceToStadium, int distanceToTheatre) throws UnknownDwellingType {
        switch (type){
            case "House":
                return createHouse(address, distanceToMarket, distanceToStadium, distanceToTheatre);
            case "PentHouse":
                return createPentHouse(address, distanceToMarket, distanceToStadium, distanceToTheatre);
            case "Flat":
                return createFlat(address, distanceToMarket, distanceToStadium, distanceToTheatre);
            default:
                throw new UnknownDwellingType();
        }
    }

    /**
     *
     * @param address
     * @param distanceToMarket
     * @param distanceToStadium
     * @param distanceToTheatre
     * @return
     */
    private House createHouse (String address, int distanceToMarket, int distanceToStadium, int distanceToTheatre){
        House house = new House();
        house.setAddress(address);
        house.setDistanceToMarket(distanceToMarket);
        house.setDistanceToStadium(distanceToStadium);
        house.setDistanceToTheatre(distanceToTheatre);
        house.calculatePrice(house);
        return house;
    }

    /**
     *
     * @param address
     * @param distanceToMarket
     * @param distanceToStadium
     * @param distanceToTheatre
     * @return
     */
    private PentHouse createPentHouse (String address, int distanceToMarket, int distanceToStadium, int distanceToTheatre){
        PentHouse pentHouse = new PentHouse();
        pentHouse.setAddress(address);
        pentHouse.setDistanceToMarket(distanceToMarket);
        pentHouse.setDistanceToStadium(distanceToStadium);
        pentHouse.setDistanceToTheatre(distanceToTheatre);
        pentHouse.calculatePrice(pentHouse);
        return pentHouse;
    }

    /**
     *
     * @param address
     * @param distanceToMarket
     * @param distanceToStadium
     * @param distanceToTheatre
     * @return
     */
    private Flat createFlat (String address, int distanceToMarket, int distanceToStadium, int distanceToTheatre){
        Flat flat = new Flat();
        flat.setAddress(address);
        flat.setDistanceToMarket(distanceToMarket);
        flat.setDistanceToStadium(distanceToStadium);
        flat.setDistanceToTheatre(distanceToTheatre);
        flat.calculatePrice(flat);
        return flat;
    }

    /**
     *
     * @return
     */
    public void generateDefaultDwellingList(){
        list.clear();
        try {
            list.add(createDwelling("House", "Shevchenka St. 34", 2, 4, 2));
            list.add(createDwelling("House", "Balabana St. 24", 1, 3, 1));
            list.add(createDwelling("PentHouse", "Khorodoc'ka St. 65", 1, 5, 2));
            list.add(createDwelling("Flat", "Naukova St. 31", 2, 8, 4));
            list.add(createDwelling("Flat", "Stryis'ka St. 12", 4, 3, 7));
            list.add(createDwelling("Flat", "Stryis'ka St. 332", 2, 7, 3));
            list.add(createDwelling("Flat", "Stryis'ka St. 32", 5, 4, 1));
            list.add(createDwelling("Flat", "Stryis'ka St. 142", 6, 7, 9));
        }
        catch(UnknownDwellingType e){
            System.out.println("Unknown type of Dwelling");
            //e.printStackTrace();
        }
    }
}
