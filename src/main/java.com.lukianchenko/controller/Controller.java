package controller;

import model.Dwelling;
import model.DwellingManager;
import model.SortArgument;
import java.util.List;

/**
 *
 */
public class Controller {
    /**
     *
     * @param dwellingManager
     */
    public void createDefaultListOfDwellings(DwellingManager dwellingManager){
        dwellingManager.generateDefaultDwellingList();
    }

    /**
     *
     * @param dwellMan
     * @param arg SortArgument constant which define what sorting is used
     */
    public void sortBy(DwellingManager dwellMan, SortArgument arg){
        switch(arg){
            case PRICE:
                dwellMan.sortByPrice(dwellMan.getList());
                break;
            case STADIUM:
                dwellMan.sortByDistanceToStadium(dwellMan.getList());
                break;
            case THEATRE:
                dwellMan.sortByDistanceToTheatre(dwellMan.getList());
                break;
            case MARKET:
                dwellMan.sortByDistanceToMarket(dwellMan.getList());
                break;
        }

    }

    /**
     *
     * @param dwell
     * @param maxPrice
     * @param maxDistanceToMarket
     * @return
     */
    public List<Dwelling> showAllDwellingsTopPriceTopDistancetoMarket(DwellingManager dwell, int maxPrice, int maxDistanceToMarket){
       return  dwell.findOptimaDwelling(dwell, maxPrice, maxDistanceToMarket);
    }

    /**
     *
     * @param dwellingManager
     */
    public void showDWellings(DwellingManager dwellingManager){
        System.out.println(dwellingManager.getList());
    }
}
